﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp2.Properties;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()


        {
            InitializeComponent();


        }
        public delegate void Updatelbltest(string text);
        public delegate void UpdatelblLineal(string text, int ilineas);
        public delegate void MyDelegate();
        public delegate void MydelageteCloseMain();
        private string sDataSerialBefore = "";
        public SerialPort puertoSerie = new SerialPort();
        public static System.Windows.Forms.Timer timer1;
        public static System.Windows.Forms.Timer timer2;
        public bool Work = false;
        private string sDataSerial = "";
        public 
            Thread wRserial;

        private void button1_Click(object sender, EventArgs e)
        {
            button2.Visible = true;
            String namepuerto = "";

            foreach (string puertoDis in SerialPort.GetPortNames())
            {
          //      MessageBox.Show(puertoDis);

                namepuerto = puertoDis;
            }

            // ClassSerialPort.SerialPortSetup.PortName = text
            //;

            ClassSerialPort.SerialPortSetup = this.puertoSerie;
            ClassSerialPort.SerialPortSetup.PortName = namepuerto;
            ClassSerialPort.SerialPortSetup.BaudRate = 115200;
            ClassSerialPort.SerialPortSetup.Handshake = Handshake.None;
            ClassSerialPort.SerialPortSetup.DataBits = 8;
            ClassSerialPort.SerialPortSetup.Parity = Parity.None;
            ClassSerialPort.SerialPortSetup.StopBits = StopBits.One;
            ClassSerialPort.iSaveSetup = 1;

          


            AbrirPuertoSerie();

            timer1 = new System.Windows.Forms.Timer();
            //this.wRserial = new Thread(new ThreadStart(this.port_DataReceived_Serial));
            //Form1.timer1.Interval = 60000 / Convert.ToInt32(Math.Truncate(Convert.ToDouble(120)));
            Form1.timer1.Interval = 500;
            Form1.timer1.Start();
            timer1.Tick += new EventHandler(this.vSendCaracterControl);
            this.wRserial = new Thread(new ThreadStart(this.port_DataReceived_Serial));
          


            timer2 = new System.Windows.Forms.Timer();
            Form1.timer2.Interval = 1000;
            //funciona Form1.timer2.Interval = 1000;
            Form1.timer2.Start();
            timer2.Tick += new EventHandler(this.timer2_Tick);
         

        }
        //public byte[] ParseDatos(string datos)
        //{


        //    List<byte> list = new List<byte>();
        //   // lbltest.Text = datos;

        //    string[] array = datos.Split(new char[]
        //    {
        //        '¬'
        //    });
        //    string[] array2 = array;
        //    for (int i = 0; i < array2.Length; i++)
        //    {
        //        string s = array2[i];
        //        byte[] bytes = Encoding.ASCII.GetBytes(s);
        //        byte[] array3 = bytes;
        //        for (int j = 0; j < array3.Length; j++)
        //        {
        //            byte item = array3[j];
        //            list.Add(item);
        //        }
        //    }
        //    return list.ToArray();
        //   }


        private void vstartTimerR()
        {
            if (base.InvokeRequired)
            {
             
                    timer2.Stop();
                    timer2.Start();
                    timer1.Stop();
                
                
            }
        }


        private void timer2_Tick(object sender, EventArgs e)
        {
            string text = "";
           // byte[] array = this.ParseDatos(this.sDataSerial);
           // lbltest.Text = sDataSerial;
          //  byte[] array2 = array;
          
            //for (int i = 0; i < array2.Length; i++)
            //{
            //    byte b = array2[i];
            //    text = text + b.ToString("X2") + " ";
            //}
            //ClassAsciiHex.ssHex = text;
            //ClassAsciiHex.ssAscii = this.sDataSerial;
            //string text2 = "";
            //int j;
            //if (1 == 0)
            //{
            //    string[] array3 = this.sDataSerial.Split(new char[]
            //    {
            //        '\r'
            //    });
            //    for (j = 0; j < array3.Length; j++)
            //    {
            //        text2 = text2 + array3[j] + Environment.NewLine;
            //    }
            //}
            //else
            //{
            //    string[] array3 = this.sDataSerial.Split(new char[]
            //    {
            //        '\r'
            //    });
            //    j = 0;
            //    while (j < 1 && j < array3.Length)
            //    {
            //        text2 = text2 + array3[j] + Environment.NewLine;
            //        j++;
            //    }
            //}


            //this.vUpdatelblLineal(sDataSerial, 1);
            //this.lbltest.Text = sDataSerial;
            
           // this.vUpdatelbltest(sDataSerial, 1);
         
           
            this.sDataSerialBefore = this.sDataSerial;
            this.sDataSerial = "";
            timer1.Stop();
            timer1.Start();
            timer2.Stop();
            timer2.Start();
            
            this.lbltest.Text = sDataSerialBefore;

        }


        private void vUpdatelbltest(string msg, int ilineas)
        {
            if (base.InvokeRequired)
            {
                object[] args = new object[]
                {
                    msg,
                    ilineas
                };
                //this.txtDisplay.BeginInvoke(new fMain.UpdatelblLineal(this.OnUpdatelLineal), args);
            }
            else
            {
                this.OnUpdatelLinealtest(msg, ilineas);
            }
        }


        private void OnUpdatelLinealtest(string msg, int ilineas)
        {
            switch (ilineas)
            {
                case 0:
                    this.lbltest.Font = new Font("Microsoft Sans Serif", 35f);

                    this.lbltest.Text = msg;

                    break;
                case 1:
                    this.lbltest.Font = new Font("Microsoft Sans Serif", 55f);

                    this.lbltest.Text =  msg;

                    break;
                case 2:
                    this.lbltest.Font = new Font("Microsoft Sans Serif", 55f);

                    this.lbltest.Text = msg;

                    break;
                case 3:
                    this.lbltest.Font = new Font("Microsoft Sans Serif", 50f);

                    this.lbltest.Text = msg;

                    break;
                case 4:
                    this.lbltest.Font = new Font("Microsoft Sans Serif", 40f);

                    this.lbltest.Text = msg;

                    break;
                default:
                    this.lbltest.Font = new Font("Microsoft Sans Serif", 35f);

                    this.lbltest.Text = msg;

                    break;
            }
        }

        private void vUpdatelblLineal(string msg, int ilineas)
        {
            if (base.InvokeRequired)
            {
                object[] args = new object[]
                {
                    msg,
                    ilineas
                };
             //   this.txtDisplay.BeginInvoke(new fMain.UpdatelblLineal(this.OnUpdatelLineal), args);
            }
            else
            {
                this.OnUpdatelLinealtest(msg, 1);
               //this.OnUpdatelLineal(msg,1);
            }
        }
        internal class ClassAsciiHex
        {
            private static string sAscii = "";
            private static string sHex = "";
            public static string ssAscii
            {
                get
                {
                    return ClassAsciiHex.sAscii;
                }
                set
                {
                    ClassAsciiHex.sAscii = value;
                }
            }
            public static string ssHex
            {
                get
                {
                    return ClassAsciiHex.sHex;
                }
                set
                {
                    ClassAsciiHex.sHex = value;
                }
            }
        }
        private void vSendCaracterControl(object sender, EventArgs e)
        {
            //this.puertoSerie.Write(new byte[] { 13, 10 }, 0, 2); 
            for (int i = 0; i < 1; i++)
            {
                //erro cunado la bascula enta apapaga
                try
                {
                    this.puertoSerie.Write("P");
                }

                catch (Exception )
                {
                    MessageBox.Show("");
                }
                ;
           }

            
        }
        private void port_DataReceived_Serial()
        {
            byte value = 0;
            this.Work = true;
            while (this.Work)
            {
                try
                {
                   value = Convert.ToByte(this.puertoSerie.ReadByte());
                    //  Form1.vstartTimerR();
                }
                catch (InvalidOperationException)
                {
                    this.Work = false;

                }
                catch (TimeoutException)
                {
                    this.Work = false;
                }
                catch (IOException)
                {
                    this.Work = false;

                }

                this.sDataSerial += Convert.ToChar(value);
            }

        }













        public void AbrirPuertoSerie()
        {
            if (puertoSerie.IsOpen)
            {
                Form1.timer1.Stop();
                Form1.timer2.Stop();
                this.puertoSerie.Close();
                while (this.Work)
                {
                    Thread.Sleep(10);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(this.puertoSerie.PortName))
                {

                    MessageBox.Show("Seleccione un puerto para crear conexión.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                else
                {
                    try
                    {
                        this.puertoSerie.Open();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("El puerto seleccionado " + this.puertoSerie.PortName + " no está disponible.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                }
                //aquie sale
            }

            wRserial = new Thread(new ThreadStart(this.port_DataReceived_Serial));

            this.wRserial.Start();
            Console.WriteLine("wRserial: Starting worker thread...");
            while (!this.wRserial.IsAlive)
            { }
        }

        private void button2_Click(object sender, EventArgs e)
        {

            this.puertoSerie.Close();
            timer1.Stop();
            timer2.Stop();
            button1.Visible = true;
            button2.Visible = false;



        }
    }










    internal class ClassSerialPort
    {
        private static SerialPort sSerialPortSeutp;
        private static int iPortSelect;
        private static int iActionSaveSetup;
        private static int posX;
        private static int posY;
        public static SerialPort SerialPortSetup
        {
            get
            {
                return ClassSerialPort.sSerialPortSeutp;
            }
            set
            {
                ClassSerialPort.sSerialPortSeutp = value;
            }
        }
        public static int iSetupPort
        {
            get
            {
                return ClassSerialPort.iPortSelect;
            }
            set
            {
                ClassSerialPort.iPortSelect = value;
            }
        }
        public static int iSaveSetup
        {
            get
            {
                return ClassSerialPort.iActionSaveSetup;
            }
            set
            {
                ClassSerialPort.iActionSaveSetup = value;
            }
        }
        public static int iposX
        {
            get
            {
                return ClassSerialPort.posX;
            }
            set
            {
                ClassSerialPort.posX = value;
            }
        }
        public static int iposY
        {
            get
            {
                return ClassSerialPort.posY;
            }
            set
            {
                ClassSerialPort.posY = value;
            }
        }
    }
   

    //internal class ClassSerialPort
    //{
    //    public static SerialPort SerialPortSetup
    //    {
    //        get
    //        {
    //            return ClassSerialPort.sSerialPortSeutp;
    //        }
    //        set
    //        {
    //            ClassSerialPort.sSerialPortSeutp = value;
    //        }
    //    }
    //public static int iSetupPort
    //{
    //    get
    //    {
    //        return ClassSerialPort.iPortSelect;
    //    }
    //    set
    //    {
    //        ClassSerialPort.iPortSelect = value;
    //    }
    //}
    //public static int iSaveSetup
    //{
    //    get
    //    {
    //        return ClassSerialPort.iActionSaveSetup;
    //    }
    //    set
    //    {
    //        ClassSerialPort.iActionSaveSetup = value;
    //    }
    //}

    //  }
}